<!-- Start Meta -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A catchall for reusable meta -->
@endif
<div class="meta"  is="fir-meta">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="meta__wrap">
      Pinecone: Meta / Meta
  </div>
</div>
<!-- End Meta -->