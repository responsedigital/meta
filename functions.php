<?php

namespace Fir\Pinecones\Meta;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Meta',
            'label' => 'Pinecone: Meta',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A catchall for reusable meta"
                ],
                [
                    'label' => 'Info',
                    'name' => 'infoTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'instructions' => 'To reuse this in any component below, enter meta:title',                    
                    'type' => 'text'
                ],
                [
                    'label' => 'Description',
                    'name' => 'description',
                    'instructions' => 'To reuse this in any component below, enter meta:description',                    
                    'type' => 'textarea'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
