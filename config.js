module.exports = {
    'name'  : 'Meta',
    'camel' : 'Meta',
    'slug'  : 'meta',
    'dob'   : 'N/A',
    'desc'  : 'A catchall for reusable meta',
}